# SHOPTECH Website - Personal project 

## Overview
- Application architecture microservices - is an architectural style the structure an app as a\
collection of services that are: independently, loosely coupled, highly maintainable and testable\
reference link [Microservices](https://microservices.io/).
- The services (modules) consist of 
[eureka-server](https://gitlab.com/spring-tech-group/spring-tech/-/tree/main/spring-tech-feature/eureka-server), [inventory](https://gitlab.com/spring-tech-group/spring-tech/-/tree/main/spring-tech-feature/inventory),
[login](https://gitlab.com/spring-tech-group/spring-tech/-/tree/main/spring-tech-feature/login), 
[order](https://gitlab.com/spring-tech-group/spring-tech/-/tree/main/spring-tech-feature/order). 
- Technologies: Spring Boot, MySQL, REST API

## Use case diagram
1. Customer side \
![Use case customer](https://i.postimg.cc/Gh2LSzkk/cus.png)

2. Employee side \
![Use case employee](https://i.postimg.cc/VNpSRpSZ/employee.png)

3. Administrator side \
![Use case admin](https://i.postimg.cc/bwWgDz0s/admin.png)

Note: [*Other diagrams*](https://lucid.app/lucidchart/1c07ffc9-978e-47cd-8d26-3638b26e35f5/edit?viewport_loc=1379%2C3032%2C6842%2C3026%2C0_0&invitationId=inv_a505d275-2863-473e-80f8-e4b150398b33)