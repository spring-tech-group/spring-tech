package com.shop.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shop.service.UserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 */
@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = true
)
@RequiredArgsConstructor
@Slf4j
public class WebSecurity {
    private final UserDetailsServiceImpl userDetailsService;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{

        log.info("filterChain: HttpSecurity");

//         Get AuthenticationManager
        JWTAuthenticationFilter jwtAuthenticationFilter =
                new JWTAuthenticationFilter(authenticationProvider());
        jwtAuthenticationFilter.setFilterProcessesUrl("v1/api/login");

        // Disable CSRF and enable CORS
        http.csrf().disable();
        http.cors();

        // Set  session management to stateless
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Set unauthorized requests exception handler.
//        http.exceptionHandling()
//                .authenticationEntryPoint(
//                        ((request, response, authException) -> {
//
//                            log.error("Unauthorized requests exception handler");
//
//                            response.sendError(
//                                    HttpServletResponse.SC_UNAUTHORIZED,
//                                    authException.getMessage()
//                            );
//                        })
//                );

        // Set permissions on endpoints
        // Our public endpoints
        http.authorizeRequests()
                .antMatchers("api/login/**", "/api/token/refresh/**", "/api/v1/**")
                .permitAll();
        // Out private endpoints
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET,"/api/user/**")
                .hasAnyAuthority("ROLE_SUPER_ADMIN");
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST,"/api/user/save/**")
                .hasAnyAuthority("ROLE_ADMIN");
//        http.authorizeRequests()
//                .antMatchers(HttpMethod.POST, "/api/login")
//                .hasAnyRole("ROLE_ADMIN", "ROLE_SUPER_ADMIN");
        http.authorizeRequests()
                .anyRequest().authenticated();

        http.authenticationProvider(authenticationProvider());

        // Add JWT token filter
        http.addFilter(jwtAuthenticationFilter);
        http.addFilterBefore(new CustomerAuthorizationFilter(),
                UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Instead of using filterChain(AuthenticationManagerBuilder auth): we export
     * DaoAuthenticationProvider bean (child of AuthenticationProvider) and pass
     * it to HttpSecurity's authenticationProvider() method.
     *
     * The standard and most common implementation is the DaoAuthenticationProvider,
     * which retrieves the user details from a simple, read-only user DAO, the
     * UserDetailsService.
     */

    // Child of AuthenticationProvider
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();

        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());

        return authProvider;
    }


    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authConfiguration)
            throws Exception {

        return authConfiguration.getAuthenticationManager();
    }

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring()
                .antMatchers("/images/**", "/js/**", "/webjars/**");
    }
}
