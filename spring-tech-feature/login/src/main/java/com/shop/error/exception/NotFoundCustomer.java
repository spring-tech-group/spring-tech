package com.shop.error.exception;

public class NotFoundCustomer extends RuntimeException{

    public NotFoundCustomer(String mess) {
        super(mess);
    }
}
