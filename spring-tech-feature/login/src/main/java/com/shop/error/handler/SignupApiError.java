package com.shop.error.handler;

import lombok.Data;

/**
 * Class handles error when registers new users, which exited before in database
 *
 * @author thng1642
 * @version 1.00
 *
 * LOG MODIFYING
 * AUTHOR       DATE            DESCRIPTION
 * =================================================================================
 * thng1642     16 Aug 2024     Creating class
 */
@Data
public class SignupApiError {

}
