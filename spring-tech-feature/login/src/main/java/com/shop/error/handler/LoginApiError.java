package com.shop.error.handler;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

/**
 * Class for representing API errors which to hold relevant information about errors
 * during REST calls.
 *
 * @author thng1642
 * @version 1.00
 *
 * LOG MODIFYING
 * AUTHOR       DATE            DESCRIPTION
 * =================================================================================
 */
@Data
public class LoginApiError {

    private HttpStatus status;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private String message;

    private LoginApiError() {
        timestamp = LocalDateTime.now();
    }

    LoginApiError(HttpStatus status) {
        this();
        this.status = status;
    }

    LoginApiError(HttpStatus status, String message) {
        this();
        this.status = status;
        this.message = message;
    }
}
