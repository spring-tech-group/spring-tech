package com.shop.error.handler;

import com.shop.error.exception.NotFoundCustomer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * This class handle the exceptions
 *
 * @author thng1642
 * @version 1.00
 *
 * Modifying Log
 * AUTHOR       DATE            DESCRIPTION
 * ===============================================================
 */
@ControllerAdvice
public class AuthRestExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * The method handles an exception account not found in db (not exited)
     * @param ex data
     * @return Response
     */
    @ExceptionHandler(NotFoundCustomer.class)
    protected ResponseEntity<Object> handleNotFoundAccount(NotFoundCustomer ex) {
        LoginApiError error = new LoginApiError(HttpStatus.NOT_FOUND);
        error.setMessage(ex.getMessage());
        return buildResponseEntity(error);
    }

//    @ExceptionHandler(UsernameNotFoundException.class)
//    protected ResponseEntity<Object> handleNotFoundAccount(UsernameNotFoundException ex) {
//        LoginApiError error = new LoginApiError(HttpStatus.NOT_FOUND);
//        error.setMessage(ex.getMessage());
//        return buildResponseEntity(error);
//    }
    private ResponseEntity<Object> buildResponseEntity(LoginApiError error){
        return new ResponseEntity<>(error, error.getStatus());
    }
}
