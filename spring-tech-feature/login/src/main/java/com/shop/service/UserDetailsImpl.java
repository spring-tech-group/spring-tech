package com.shop.service;

import com.shop.model.AccountApp;
import net.minidev.json.annotate.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class UserDetailsImpl implements UserDetails {

    private Integer id;

    private String username;

    private String name;

    @JsonIgnore
    private String password;

    private boolean enable;

    // Refers to the permission of the authenticated user.
    private Collection<? extends GrantedAuthority> authorities;

    public UserDetailsImpl(Integer id, String username, String name, String password,
                           Collection<? extends GrantedAuthority> authorities, boolean enable) {

        this.id = id;
        this.username = username;
        this.name = name;
        this.password = password;
        this.authorities = authorities;
        this.enable = enable;
    }

    public static UserDetailsImpl build(AccountApp cus) {

        List<GrantedAuthority> authorities = cus.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());

        return new UserDetailsImpl(
                cus.getUserId(),
                cus.getUsername(),
                cus.getName(),
                cus.getPassword(),
                authorities,
                cus.isEnable());
    }

    public String getName() {
        return name;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return authorities;
    }

    @Override
    public String getPassword() {

        return password;
    }

    @Override
    public String getUsername() {

        return username;
    }

    @Override
    public boolean isAccountNonExpired() {

        return true;
    }

    @Override
    public boolean isAccountNonLocked() {

        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {

        return true;
    }

    @Override
    public boolean isEnabled() {

        return true;
    }

}
