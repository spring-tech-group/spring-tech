package com.shop.service;

import com.shop.model.AccountApp;
import com.shop.model.Role;

import java.util.List;

public interface CustomerService {

    AccountApp saveUser(AccountApp cus);
    Role saveRole(Role role);
    void addRoleToUser(String username, String roleName);
    AccountApp getUser(String username);
    List<AccountApp> getUsers();
    // Signup new account for user
    boolean signupUser(String email, String password);
//    VerificationToken saveVerificationToken(VerificationToken token);

//    VerificationToken getVerificationToken(int id);

//    void checkingToken(String code, HttpServletRequest req, HttpServletResponse res)
//            throws IOException;

//    void senderVerificationEmail(NewGuestReq req, String url);
}
