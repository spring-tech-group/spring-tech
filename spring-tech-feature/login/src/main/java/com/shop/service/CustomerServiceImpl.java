package com.shop.service;

import com.shop.model.AccountApp;
import com.shop.model.Role;
import com.shop.repository.CustomerRepo;
import com.shop.repository.RoleRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final PasswordEncoder passwordEncoder;

    private final CustomerRepo customerRepo;

    private final RoleRepo roleRepo;

    @Override
    public AccountApp saveUser(AccountApp cus) {

        log.info("Saving new customer {} to the database", cus.getUsername());
        String plainPassword = cus.getPassword();
        cus.setPassword(passwordEncoder.encode(plainPassword));

        return customerRepo.saveAndFlush(cus);
    }

    @Override
    public Role saveRole(Role role) {
        log.info("Saving new ROLE {} to the database", role.getName());

        return roleRepo.save(role);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {

        log.info("Adding ROLE {} to USER {} ", roleName, username);

        AccountApp userApp = customerRepo.findByUsername(username)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User Not Found with -> username or email :"
                                + username));

        Role role = roleRepo.findByName(roleName);

        userApp.getRoles().add(role);
    }

    @Override
    public AccountApp getUser(String username) {

        log.info("Fetching user {}", username);

        return customerRepo.findByUsername(username).orElseThrow(
                () -> new UsernameNotFoundException("User Not Found with -> username or email : "
                        + username));
    }

    @Override
    public List<AccountApp> getUsers() {

        log.info("Fetching all users");

        return customerRepo.findAll();
    }

    @Override
    public boolean signupUser(String email, String password) {

        // check email exited before?
        boolean isExited = !customerRepo.findByUsername(email).isEmpty();

        if (isExited == true) {
            return false;
        } else {
            AccountApp  newAcc = AccountApp.builder()
                .username(email)
                .enable(true)
                .password(password)
                .roles(new ArrayList<>())
                .build();
            saveUser(newAcc);
            addRoleToUser(email, "ROLE_USER");
            return true;
        }
    }
}
