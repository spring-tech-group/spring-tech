package com.shop.service;

import com.shop.error.exception.NotFoundCustomer;
import com.shop.model.AccountApp;
import com.shop.repository.CustomerRepo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
@AllArgsConstructor
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService  {

    private final CustomerRepo customerRepo;

    @Override
    public UserDetails loadUserByUsername(String username)  {

        AccountApp user =
                customerRepo.findByUsername(username).orElseThrow(
                () -> new NotFoundCustomer(format("Tài khoản %s không tồn tại", username)));

        UserDetailsImpl result = UserDetailsImpl.build(user);

        return result;
    }
}
