package com.shop.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.shop.domain.dto.AuthRequest;
import com.shop.domain.dto.AuthResponse;
import com.shop.service.CustomerService;
import com.shop.service.UserDetailsImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toCollection;

/**
 * Authentication and authorization user access system
 *
 * @author thng1642
 * @version 1.00
 *
 * Modification log
 * Date         Author          Description
 * ===================================================
 * 10 Feb 2023  thng1642        Refactor code
 * 19 Aug 2024  thng1642        Adding api for register
 *                              new user
 */
@RestController
@RequestMapping("/api/v1/")
@RequiredArgsConstructor @Slf4j
public class AuthApi {

    private final AuthenticationManager authenticationManager;
    private final CustomerService service;

    @PostMapping("login")
    public ResponseEntity<?> login(@RequestBody AuthRequest request) {
        try {

            Authentication authentication = authenticationManager
                    .authenticate(
                            new UsernamePasswordAuthenticationToken(request.username()
                                    ,request.password()));
            UserDetailsImpl account = (UserDetailsImpl)authentication.getPrincipal();

            List<GrantedAuthority> authorities = account.getAuthorities()
                    .stream().collect(toCollection(ArrayList::new));

            Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());

            // Generate access jwt from username.
            String access_token = JWT.create()
                    .withSubject(account.getUsername())
                    .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000))           //expiration is 10 minutes.
//                    .withIssuer(req.getRequestURL().toString())
                    .withClaim("roles", account.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                    .sign(algorithm);

            String refresh_token = JWT.create()
                    .withSubject(account.getUsername())
                    .withExpiresAt(new Date(System.currentTimeMillis() + 30 * 60 * 1000))           //expiration is 30 minutes.
//                    .withIssuer(req.getRequestURL().toString())
                    .sign(algorithm);

//            for (GrantedAuthority item : authorities) {
//                if (item.getAuthority().equals("ROLE_USER")) {
//                    throw new BadCredentialsException("Bạn không có quyền try cập hệ thống");
//                }
//
//            }
            // return access_token and refresh token

            AuthResponse response = new AuthResponse(account.getUsername(),
                    account.getName(), authorities, access_token, refresh_token);

            return ResponseEntity.ok().body(response);

        }
        catch (BadCredentialsException ex) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
        }
    }

    @PostMapping("signup")
    public ResponseEntity<?> SignupUser(@RequestBody AuthRequest request) {
        boolean result = service.signupUser(request.username(), request.password());
        if (result) return ResponseEntity.ok().body("Đăng ký thành công");
        return ResponseEntity.badRequest().body("Tài khoản đã tồn tại");
    }
}
