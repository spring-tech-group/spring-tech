package com.shop.domain.dto;

import org.springframework.security.core.GrantedAuthority;

import java.util.List;

public record AuthResponse(String username, String name,
                           List<GrantedAuthority> authorities,
                           String access_token,
                           String refresh_token) {
}
