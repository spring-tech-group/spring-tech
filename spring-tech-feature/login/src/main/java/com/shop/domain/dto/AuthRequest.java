package com.shop.domain.dto;

public record AuthRequest (String username, String password) {
}
