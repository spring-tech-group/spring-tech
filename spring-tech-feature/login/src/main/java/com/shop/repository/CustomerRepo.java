package com.shop.repository;

import com.shop.model.AccountApp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepo extends JpaRepository<AccountApp, Integer> {

    Optional<AccountApp> findByUsername(String username);
}
