
## Overview
- Using Spring Security + Spring Boot, JWT token
- Function: login, logout, authorization, authentication, ...
- Authentication for user login system: admin, client, ...
## API
| API               | Method |
|-------------------|--------|
| api/v1/login      | POST   |
| api/refresh/token | GET    |
| api/v1/signup     | POST|
## Spring Security
- Powerful and customizable authentication and authorization framework.
### Defining Terminology
- **Authentication** refers to the process of verifying the identity of a user, based on\
provided credentials (username & password). You can think of it as answer to the question\
_Who are you?_
- **Authorization** refers to the process of determining if a user has proper permission to\
perform a particular action or read particular data, assuming that the user is successfully\
authenticated. _Can a user do/read this?_
- **Granted authority** refers to the permission of the authentication user.
- **Role** refers to group of permission of the authenticated user.
### Architecture overview
1. **Spring Security Filters Chain**: intercepts all incoming requests. This chain consists of\
various filters, and each of them handles a particular use case.
2. **AuthenticationManage**: as a coordinator where you can register multiple providers, and\
based on the request type, it'll deliver an authentication request to the correct _provider_.
3. **AuthenticationProvider**: processes specific types of authentication, 
two function:_authenticate_-performs authentication with the request; _support_ checks if\
provider supports the indicated authentication type.
4. **UserDetailsService**: is described as core interface that loads user-specific data.
