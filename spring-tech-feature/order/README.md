## Overview
- Selecting location receives items
- Making orders items of customer.
- Cancel orders.
- Checking status order, so on.
## API
| api (api/v1) | method |
|--------------|--------|
| /cities      | GET    |
| /districts   | GET    |
| /communes    | GET    |
| /order       | POST   |