package com.order.service.order;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.order.domain.dto.*;
import com.order.domain.model.Address;
import com.order.domain.model.Customer;
import com.order.domain.model.Item;
import com.order.domain.model.OrderItems;
import com.order.repository.AddressRepo;
import com.order.repository.CustomerRepo;
import com.order.repository.ItemRepo;
import com.order.repository.OrderRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service @Slf4j @RequiredArgsConstructor @Transactional
public class OrderServiceImpl implements OrderService {

    private final CustomerRepo customerRepo;

    private final OrderRepo orderRepo;

    private final AddressRepo addressRepo;

    private final RestTemplate restTemplate;


    private final ItemRepo itemRepo;

    private final ObjectMapper objectMapper;

    @Override
    public OrderResponse makeOrder(OrderDto req) {

        String inventoryUrl = "http://INVENTORY/api/v1/items-check";

        // Create body request by wrapping the object in HttpEntity
        HttpEntity<List<ItemDto>> request = new HttpEntity<List<ItemDto>>(req.getItems());

        // send request to inventory with body in HttpEntity for HTTP POST
        ResponseEntity<List> response = restTemplate
                .postForEntity(inventoryUrl, request, List.class);

        List<CheckItemsResponse> checkItemsResponses = (List<CheckItemsResponse>) response.getBody();

        List<CheckItemsResponse> results = new ArrayList<>();

        try {
            int n = checkItemsResponses.size();
            for(int i = 0; i < n; i++) {

                CheckItemsResponse tmp = objectMapper.convertValue(checkItemsResponses.get(i)
                        , CheckItemsResponse.class);
                results.add(tmp);
            }

        } catch (Exception e) {

            throw new RuntimeException(e.getMessage());
        }

        // fail order
        for (CheckItemsResponse i : results) {
            if (i.getTheRemainsOfItem() < 0) {
                log.info("Đặt hàng thất bại.");

                return new OrderResponse(false, "Hết hàng tạm thời.", null);
            }
        }

        // successful order
        Optional<Customer> customer = customerRepo
                .getCustomerOrdered(req.getCustomer().getEmail());
        OrderItems orderItems =  OrderItems.builder()
                .createAt(req.getCreateAt())
                .totalPrice(req.getTotalPrice())
                .status(req.getStatus())
                .build();

        Customer customer1 = new Customer();

        if (customer.isEmpty()) {

            CustomerDto tmpCus = req.getCustomer();

            customer1 = Customer.builder()
                    .email(tmpCus.getEmail())
                    .phoneNumber(tmpCus.getPhoneNumber())
                    .fullName(tmpCus.getFullName())
                    .build();
            orderItems.setCustomer(customer1);
        }
        else {
            orderItems.setCustomer(customer.get());
        }
        List<ItemDto> reqItems = req.getItems();
        List<Item> items = new ArrayList<>(reqItems.size());

        orderRepo.saveAndFlush(orderItems);

        for (ItemDto x : reqItems) {
            Item tmp = new Item();
            tmp.setName(x.getName());
            tmp.setDeviceId(x.getDeviceId().toString());
            tmp.setOptionId(x.getOptionId().toString());
            tmp.setQuantity(x.getQuantity());
            tmp.setTotalPrice(x.getTotalPrice());
            tmp.setOrderItems(orderItems);
            items.add(tmp);
        }

        itemRepo.saveAllAndFlush(items);

        AddressDto tmpAddress = req.getAddress();

        Address address = Address.builder()
                .orderItems(orderItems)
                .city_id(tmpAddress.getCityId())
                .district_id(tmpAddress.getDistrictId())
                .commune_id(tmpAddress.getCommuneId())
                .details(tmpAddress.getDetails())
                .build();

        addressRepo.saveAndFlush(address);

        return new OrderResponse(true, "Đặt hàng thành công", orderItems.getId());
    }

    @Override
    public boolean cancelOrderItems(Integer orderId) {

        orderRepo.deleteById(orderId);
        Integer isDel = addressRepo.deletingAddress(orderId);

        if (isDel != 0) return true;
        return false;
    }

    @Override
    public Integer getIdCustomerByEmail(String email) {
        Customer customer = customerRepo.getCustomerOrdered(email).orElseThrow(
                () -> new RuntimeException("Not found customer " + email)
        );
        return customer.getId();
    }

    @Override
    public List<OrderHistoryResponse> getOrderHistory(Integer customerId) {

        List<OrderItems> orderItemsList = orderRepo.getHistoryOrdered(customerId);

        List<OrderHistoryResponse> resHistory = new ArrayList<>(orderItemsList.size());

        for (OrderItems tmp : orderItemsList) {
            // set id, datetime, status
            OrderHistoryResponse ele = new OrderHistoryResponse();
            ele.setId(tmp.getId());
            ele.setTime(tmp.getCreateAt().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
            ele.setStatus(tmp.getStatus());

            // set list item, which along to order id
            List<ItemResponse> items = new ArrayList<>();
            List<IItemResponse> resultQuery = itemRepo.getAllItemsOfOrder(tmp.getId());

            for (IItemResponse x : resultQuery) {

                items.add(new ItemResponse(x.getDevice_id(), x.getName(), x.getQuantity(), x.getTotal_price()));
            }
            ele.setItemsResponse(items);

            resHistory.add(ele);
        }

        return resHistory;
    }
}
