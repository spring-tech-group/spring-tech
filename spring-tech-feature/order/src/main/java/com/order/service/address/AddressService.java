package com.order.service.address;

import com.order.domain.model.City;
import com.order.domain.model.Commune;
import com.order.domain.model.District;

import java.util.List;

/**
 * Defines methods for selection address
 *
 * @author thng1642
 * @version 1.00
 *
 * Modification logs
 * Date         Author          Description
 * ===========================================================
 *
 */
public interface AddressService {
    /**
     * get all cities at Vietnam
     * @return list of city
     */
    List<City> getAllCities();

    /**
     * get all districts of city
     * @return list of district
     */
    List<District> getDistrictsOfCity(Integer cityId);

    /**
     * get all communes of district
     * @return list of commune
     */
    List<Commune> getAllCommunesOfDistrict(Integer districtId);
}
