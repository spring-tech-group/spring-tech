package com.order.service.order;

import com.order.domain.dto.OrderDto;
import com.order.domain.dto.OrderHistoryResponse;
import com.order.domain.dto.OrderResponse;

import java.util.List;

/**
 * @author thng1642
 * @version 1.00
 *
 * Modification logs
 * Date         Author                  Description
 * =========================================================
 * 14 Feb 2023  thng1642                Adding function get
 *                                      orders history.And
 *                                      function getIdCustomerByEmail
 */
public interface OrderService {

    /**
     * Marking order items
     * @param req request from client
     * @return response to customer
     */
    OrderResponse makeOrder(OrderDto req);

    /**
     * Cancel order
     * @param orderId id of order
     */
    boolean cancelOrderItems(Integer orderId);

    List<OrderHistoryResponse> getOrderHistory(Integer customerId);

    Integer getIdCustomerByEmail(String email);
}
