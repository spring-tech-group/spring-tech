package com.order.service.address;

import com.order.domain.model.City;
import com.order.domain.model.Commune;
import com.order.domain.model.District;
import com.order.repository.CityRepo;
import com.order.repository.CommuneRepo;
import com.order.repository.DistrictRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService{

    @Autowired CityRepo cityRepo;

    @Autowired DistrictRepo districtRepo;

    @Autowired CommuneRepo communeRepo;

    @Override
    public List<City> getAllCities() {
        return cityRepo.getAllCities();
    }

    @Override
    public List<District> getDistrictsOfCity(Integer cityId) {

        List<District> results = districtRepo.getDistrictOfCity(cityId).orElseThrow(
                () -> new RuntimeException("Not found districts!")
        );
        return results;
    }

    @Override
    public List<Commune> getAllCommunesOfDistrict(Integer districtId) {

        List<Commune> results = communeRepo.getAllCommunesOfDistrict(districtId).orElseThrow(
                () -> new RuntimeException("Not found communes!")
        );
        return results;
    }
}
