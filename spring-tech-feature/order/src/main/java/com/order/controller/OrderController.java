package com.order.controller;

import com.order.domain.dto.OrderDto;
import com.order.domain.dto.OrderHistoryResponse;
import com.order.service.order.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Making orders items for customer
 *
 * @author thng1642
 * @version 1.00
 *
 * Modification logs
 * Date         Author              Description
 * =======================================================
 * 13 Feb 2023  thng1642            Adding comments
 * 14 Feb 2023  thng1642            Adding api get ordered history
 */
@RestController @RequiredArgsConstructor
@RequestMapping("api/v1")
public class OrderController {

    private final OrderService orderService;

    @PostMapping("/order")
    public ResponseEntity<?> makeOrderItems(@RequestBody OrderDto req) {

        return ResponseEntity.ok(orderService.makeOrder(req));
    }

    @DeleteMapping("/huydon")
    public ResponseEntity<?> cancelOrderItems(@RequestParam("order")Integer orderId) {
        orderService.cancelOrderItems(orderId);
        return ResponseEntity.ok("Ok, deleting");
    }

    @GetMapping("/lichsu")
    public ResponseEntity<?> getOrderedHistory(@RequestParam("khachhang")String email) {
        Integer cusId = orderService.getIdCustomerByEmail(email);
        List<OrderHistoryResponse> res = orderService.getOrderHistory(cusId);

        return ResponseEntity.ok(res);
    }
}
