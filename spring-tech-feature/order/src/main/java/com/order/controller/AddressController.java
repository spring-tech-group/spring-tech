package com.order.controller;

import com.order.service.address.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.*;

/**
 * Selecting location received items
 *
 * @author thng1642
 * @version 1.00
 *
 * Modification logs
 * Date         Author              Description
 * =======================================================
 * 13 Feb 2023  thng1642            Adding comments
 */
@RestController @RequiredArgsConstructor
@RequestMapping("api/v1")
public class AddressController {

    private final AddressService addressService;

    @GetMapping("/cities")
    public ResponseEntity<?> getAllCities() {

        return ResponseEntity.ok(addressService.getAllCities());
    }

    @GetMapping("/districts")
    public ResponseEntity<?> getDistricts(@RequestParam("city")Integer cityId) {

        return ResponseEntity.ok(addressService.getDistrictsOfCity(cityId));
    }

    @GetMapping("/communes")
    public ResponseEntity<?> getCommunes(@RequestParam("district")Integer districtId){

        return  ResponseEntity.ok(addressService.getAllCommunesOfDistrict(districtId));
    }
}
