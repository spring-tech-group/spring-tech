package com.order.repository;

import com.order.domain.dto.IItemResponse;
import com.order.domain.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thng1642
 * @version 1.00
 *
 * Modification logs
 * Date         Author              Description
 * ==============================================================
 * 14 Feb 2023  thng1642            Adds query getAllItemsOfOrder
 */
@Repository
public interface ItemRepo extends JpaRepository<Item, Integer> {

    /**
     * Get all items along to order_id
     * @param orderId order_id in db
     * @return list of item along to orderId
     */
    @Query(value = "SELECT u.device_id, u.name, u.quantity, u.total_price FROM item u WHERE u.order_id = ?1",
            nativeQuery = true)
    List<IItemResponse> getAllItemsOfOrder(Integer orderId);
}
