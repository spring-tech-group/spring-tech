package com.order.repository;

import com.order.domain.model.OrderItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thng1642
 * @version 1.00
 *
 * Modification logs
 * Date         Author              Description
 * ================================================================
 * 14 Feb 2023  thng1642            Adding query gets list of the
 *                                  ordered customer
 *
 */
@Repository
public interface OrderRepo extends JpaRepository<OrderItems, Integer> {

    @Query(value = "SELECT * FROM order_items u WHERE u.customer_id = ?1"
            , nativeQuery = true)
    List<OrderItems> getHistoryOrdered(Integer customerId);
}
