package com.order.domain.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data @Table @Entity
public class Commune {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "district_id")
    private Integer districtId;

    @Column(name = "name")
    private String name;
}
