package com.order.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

@Data @NoArgsConstructor @AllArgsConstructor
public class OrderResponse {
    private boolean isOrdered;
    private String message;
    @Nullable
    private Integer id;
}
