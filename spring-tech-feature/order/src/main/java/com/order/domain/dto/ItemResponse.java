package com.order.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class is contained history items which bought by customer
 *
 * @author thng1642
 * @version 1.00
 *
 * Modification logs
 * Date         Author              Description
 * ===============================================================
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class ItemResponse {
    private Integer deviceId;
    private String name;
    private Short quantity;
    private String totalPrice;

}
