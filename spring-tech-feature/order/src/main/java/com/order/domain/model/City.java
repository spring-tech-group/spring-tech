package com.order.domain.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "city")
@Data
public class City {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "cityname")
    private String name;
}
