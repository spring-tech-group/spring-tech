package com.order.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity @Data @Builder @AllArgsConstructor @NoArgsConstructor
public class Item {

    @Id
    @SequenceGenerator(
            name = "item_id_sequence",
            sequenceName = "item_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "item_id_sequence"
    )
    private Integer itemId;

    private String name;

    private String deviceId;

    private String optionId;

    private Short quantity;

    private String totalPrice;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private OrderItems orderItems;
}
