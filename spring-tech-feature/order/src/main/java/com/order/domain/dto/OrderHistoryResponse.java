package com.order.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Response which gets all order history of customer
 *
 * @author thng1642
 * @version 1.00 14 Feb 2023
 *
 * Modification logs
 * Date         Author              Description
 * ===================================================
 *
 */
@Data @NoArgsConstructor @AllArgsConstructor
public class OrderHistoryResponse {
    private Integer id;
    private List<ItemResponse> itemsResponse;
    private String time;
    private String status;
}
