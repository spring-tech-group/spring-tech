package com.order.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class ItemDto {

    private String name;

    private Integer deviceId;

    private Integer optionId;

    private Short quantity;

    private String totalPrice;
}
