package com.order.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data @AllArgsConstructor @NoArgsConstructor
public class OrderDto {

    private Integer id;

    private LocalDateTime createAt;

    private String totalPrice;

    private String status;

    private CustomerDto customer;

    private List<ItemDto> items;

    private AddressDto address;
}
