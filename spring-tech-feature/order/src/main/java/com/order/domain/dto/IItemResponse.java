package com.order.domain.dto;

/**
 * Object contains result of query in ItemRepo
 *
 * @author thng1642
 * @version 1.00
 *
 * Modification logs
 * Date         Author              Description
 * ==================================================
 */
public interface IItemResponse {
    Integer getDevice_id();

    String getName();
    Short getQuantity();
    String getTotal_price();
}
