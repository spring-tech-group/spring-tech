package com.order.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class CustomerDto {

    private Integer id;

    private String email;

    private String fullName;

    private String phoneNumber;
}
