package com.order.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data @Entity @Builder @AllArgsConstructor @NoArgsConstructor
public class OrderItems {

    @Id
    @SequenceGenerator(
            name = "order_id_sequence",
            sequenceName = "order_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "order_id_sequence"
    )
    private Integer id;

    private LocalDateTime createAt;

    private String totalPrice;

    @Column(length = 100)
    private String status;          //

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @OneToMany(mappedBy = "orderItems", orphanRemoval = true)
    private List<Item> items;

    @OneToOne(mappedBy = "orderItems")
    private Address address;
}
