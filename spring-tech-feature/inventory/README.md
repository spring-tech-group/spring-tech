# Inventory services

## Overview
- Main function: Anything relative to product (phone, laptop,...) such as: adding, checking\
stock-out, fetching, ... is managed at this service.

## API

| API (api/v1/)    | Method | Function                                           |
|------------------|--------|----------------------------------------------------|
| create/cpu       | POST   | creates new cpu for phone                          |
| cpus             | GET    | gets all cpus existed                              |
| create/battery   | POST   | creates battery for phone                          |
| create/connect   | POST   | creates connect for phone                          |
| create/gpu/phone | POST   | creates gpu for phone                              |
| cpus/phone       | GET    | gets all cpus phone created                        |
| create/cameras   | POST   | creates camera                                     |
| create/screens   | POST   | creates a screen                                   |
| brand            | POST   | adds new brand                                     |
| all (temporary)  | GET    | gets all brands                                    |
| create/phone     | POST   | creates base info phone                            |
| update/phone     | POST   | updates specs for phone which created before       |
| phonecard        | GET    | shows up product card                              |
| phone            | GET    | gets details specs phone                           |
| items-check      | POST   | checking stock out of items which customer ordered |


