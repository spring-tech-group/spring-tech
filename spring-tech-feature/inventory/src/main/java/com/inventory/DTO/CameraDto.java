package com.inventory.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CameraDto {
    private Integer id;
    private String solution;
    private String features;
    private String position;
    private Integer phoneId;
}
