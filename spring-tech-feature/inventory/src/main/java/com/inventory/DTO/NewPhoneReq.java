package com.inventory.DTO;

import java.util.List;

public record NewPhoneReq(PhoneDto phone,
                          BatteryDto battery,
                          Integer brandId,
                          ConnectDto connect,
                          FCameraDto fcamera,
                          GpuDto gpu,
                          NewCpuReq cpu,
                          List<OptionDto> options,
                          List<RCameraDto> rCameras,
                          List<ScreenDto> screens) {
}
