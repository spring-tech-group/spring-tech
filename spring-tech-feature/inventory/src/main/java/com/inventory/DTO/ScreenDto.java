package com.inventory.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor @AllArgsConstructor
public class ScreenDto{
     Integer id;
     Short brightest;
     String dimension;
     String note;
     String resolution;
     String tech;
     Integer phoneId;
}
