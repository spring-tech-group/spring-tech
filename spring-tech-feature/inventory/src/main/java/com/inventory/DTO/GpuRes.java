package com.inventory.DTO;

public interface GpuRes {
    Integer getId();
    String getName();
}
