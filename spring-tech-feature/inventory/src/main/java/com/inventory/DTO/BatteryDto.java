package com.inventory.DTO;

public record BatteryDto(Integer id,
                         String capacity,
                         String charging,
                         String tech,
                         String type) {

}
