package com.inventory.DTO;

public interface CpuDto {

    Integer getId();
    String getName();
    Short getCore();
}
