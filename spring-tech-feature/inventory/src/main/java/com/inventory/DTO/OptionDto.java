package com.inventory.DTO;

public record OptionDto(Integer id,
                        String addPrice,
                        Integer quantity,
                        Short ram,
                        String storage) {
}
