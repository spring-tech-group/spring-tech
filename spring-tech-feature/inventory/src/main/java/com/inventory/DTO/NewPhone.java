package com.inventory.DTO;

import java.time.LocalDate;

public record NewPhone(Integer id, LocalDate dateAt,
                       String dimensions,
                       String name,
                       String operation,
                       String price,
                       String weigh,
                       Short ram,
                       String storage, Integer quantity, Integer brandId) {
}
