package com.inventory.DTO;

import java.time.LocalDate;

public interface IDeviceDto {
//    private String name;
//    private String avt;
//    private String price;
//    private LocalDate updatedDate;
    Integer getLaptop_id();
    String getName();
    String getAvt();
    String getPrice();
    LocalDate getUpdated_date();
}
