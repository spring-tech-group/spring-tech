package com.inventory.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdatePhoneDto {
    private Integer phone_id;
    private Integer battery_id;
    private Integer connect_id;
    private Integer gpu_id;
    private Integer cpu_id;
    private Integer fcamera_id;
    private PhoneImgDto phoneImgDto;
}
