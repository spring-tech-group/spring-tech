package com.inventory.DTO;

public interface BrandResp {

    Integer getId();
    String getName();
    String getLogo();
    String getField();
}
