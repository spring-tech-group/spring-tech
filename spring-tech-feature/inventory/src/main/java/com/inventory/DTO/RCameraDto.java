package com.inventory.DTO;

public record RCameraDto(Integer id,
                         String features,
                         String resolution) {
}
