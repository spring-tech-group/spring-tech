package com.inventory.DTO;

public record ConnectDto(Integer id,
                         String bluetooth,
                         String mobileNetwork,
                         String wifi) {
}
