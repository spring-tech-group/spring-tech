package com.inventory.DTO;


public record BrandDto(Integer id,
                        String name,
                       String logo,
                       String field){
}
