package com.inventory.DTO;

public record FCameraDto(Integer id,
                         String features,
                         String resolution) {
}
