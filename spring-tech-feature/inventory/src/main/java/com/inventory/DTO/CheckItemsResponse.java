package com.inventory.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class CheckItemsResponse {

    private Integer optionId;
    private Integer theRemainsOfItem;
}
