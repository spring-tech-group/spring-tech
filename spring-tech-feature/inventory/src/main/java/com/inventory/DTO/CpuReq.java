package com.inventory.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CpuReq {
    private Integer id;
    private String name;
    private Speeds[] speeds;
    private String fastest;
    private Short core;
    private Short thread;
    private Float cached;
}

