package com.inventory.DTO;

import java.util.Map;

public record  NewCpuReq(String name, Float cached, Short core, String fastest,
                         Integer id,
                         Map<String, Float> speeds,
                         Short thread,
                         Boolean isNewProducer,
                         String producerName) {
}
