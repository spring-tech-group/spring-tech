package com.inventory.DTO;

import java.time.LocalDate;

public record PhoneDto(Integer id,
                       LocalDate dateAt,
                       String dimensions,
                       String name,
                       String operation,
                       String price,
                       String weigh) {
}
