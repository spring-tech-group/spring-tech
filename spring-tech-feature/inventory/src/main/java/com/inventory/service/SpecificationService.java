package com.inventory.service;

import com.inventory.DTO.*;
import com.inventory.model.GpuPhone;

import java.util.List;
import java.util.Map;

/**
 * Services relatives with device specification
 *
 * @author thng1642
 * @version 1.00
 *
 * Modification log
 * Date         Author          Description
 * ===============================================================
 * 10 Feb 2023  thng1642        Refactor code
 */
public interface SpecificationService {
    // creates new cpu
    CpuReq createNewCpu(CpuReq req);

    // Get all cpus
    List<CpuDto> getCpus();

    // Create battery
    BatteryDto createBattery(BatteryDto req);

    // Create connect
    ConnectDto createConnect(ConnectDto req);
    // Create front camera
    List<CameraDto> createCameras(List<CameraDto> req);

    // Create new gpu
    GpuPhone createGpu(GpuDto req);
    // Get all gpus
    List<GpuRes> getAllGpus();

//    String convertMapSpeeds(Map<String, Float> map);

//    void createListOptionsPhone(OptionDto req, Integer phoneId);

//    void createRCamera(RCameraDto req, Integer phoneId);

    ScreenDto createScreenForPhone(ScreenDto req);

}
