package com.inventory.service;

import com.inventory.DTO.BrandDto;
import com.inventory.DTO.BrandResp;
import com.inventory.model.Brand;
import com.inventory.model.Producer;
import com.inventory.repository.BrandRepo;
import com.inventory.repository.ProducerRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class BrandServiceImpl implements BrandService {

//    private final ProducerRepo producerRepo;

    private final BrandRepo brandRepo;

    @Override
    public Brand createdNewBrand(BrandDto request) {

        log.info("Create new brand: {}", request.name());

        // Checking brand existed in db ?
        Integer isExisted = brandRepo.getNumberOfBrand(request.name().toLowerCase());
        if (isExisted == null || isExisted == 0) {
            Brand tmp = Brand.builder()
                    .name(request.name())
                    .logo(request.logo())
                    .field(request.field())
                    .build();
            return brandRepo.saveAndFlush(tmp);
        }
        else {
            return null;
        }
    }

    @Override
    public Integer getNumberBrand(String name) {

        return brandRepo.getNumberOfBrand(name.toLowerCase());
    }

    @Override
    public List<BrandResp> getAllBrands() {
        return brandRepo.getBrands();
    }
}
