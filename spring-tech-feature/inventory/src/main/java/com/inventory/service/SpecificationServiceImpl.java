package com.inventory.service;

import com.inventory.DTO.*;
import com.inventory.model.*;
import com.inventory.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class SpecificationServiceImpl implements SpecificationService {

    private final CpuRepo cpuRepo;

    private final BatteryRepo batteryRepo;

    private final ConnectRepo connectRepo;

    private final FCameraRepo fCameraRepo;

    private final GpuPhoneRepo gpuPhoneRepo;

    private final RCameraRepo rCameraRepo;

    private final ScreenRepo screenRepo;

    @Override
    public CpuReq createNewCpu(CpuReq req) {

        log.info("Adding new cpu: {}", req.getName());
        Speeds[] speeds = req.getSpeeds().clone();
//        log.info("Speeds: {}", convertSpeedsArr(speeds));

        CPU cpu = CPU.builder()
                .name(req.getName())
                .cached(req.getCached())
                .core(req.getCore())
                .fastest(req.getFastest())
                .speeds(convertSpeedsArr(speeds))
                .thread(req.getThread())
                .build();
        cpu = cpuRepo.saveAndFlush(cpu);
        req.setId(cpu.getCpuId());
        return req;
    }
    private String convertSpeedsArr(Speeds[] arr) {

        StringBuilder arrAsString = new StringBuilder();

        for (Speeds item : arr) {
            arrAsString.append(item.getNumberCores()+":"
                    +item.getPerformance()+"; ");
        }
        arrAsString.delete(arrAsString.length() - 2, arrAsString.length());
        return arrAsString.toString();
    }
    @Override
    public List<CpuDto> getCpus() {
        log.info("Get all cpus from db");

        return cpuRepo.getAllCpus();
    }

    @Override
    public BatteryDto createBattery(BatteryDto req) {
        log.info("Create battery");
        Battery tmp = Battery.builder()
                .type(req.type())
                .charging(req.charging())
                .tech(req.tech())
                .capacity(req.capacity())
                .build();

        batteryRepo.saveAndFlush(tmp);
        BatteryDto res = new BatteryDto(tmp.getBatteryId(), tmp.getCapacity(),
                tmp.getCharging(),tmp.getTech(), tmp.getType());

        return res;
    }

    @Override
    public ConnectDto createConnect(ConnectDto req) {

        log.info("Create new connect");
        Connect connect = Connect.builder()
                .bluetooth(req.bluetooth())
                .wifi(req.wifi())
                .mobileNetwork(req.mobileNetwork())
                .build();
        connectRepo.saveAndFlush(connect);

        ConnectDto res = new ConnectDto(connect.getConnectId(),
                connect.getBluetooth(), connect.getMobileNetwork(),
                connect.getWifi());

        return res;
    }

    @Override
    public List<CameraDto> createCameras(List<CameraDto> req) {

        log.info("Create new camera");

        for(CameraDto item : req) {
            if (item.getPosition().equals("rear")) {
                RCamera rCamera = RCamera.builder()
                        .features(item.getFeatures())
                        .resolution(item.getSolution() + " MP")
                        .build();

               rCameraRepo.saveAndFlush(rCamera);

               item.setId(rCamera.getCameraId());

               rCameraRepo.updatingPhoneIdForRCamera(item.getPhoneId(), item.getId());
            }
            else {
                FCamera fCamera = FCamera.builder()
                        .features(item.getFeatures())
                        .resolution(item.getSolution() + " MP")
                        .build();
                fCameraRepo.saveAndFlush(fCamera);
                item.setId(fCamera.getCameraId());
            }
        }

        return req;
    }

    @Override
    public GpuPhone createGpu(GpuDto req) {

        log.info("Create new GPU with name: {}", req.getName());
        String name = req.getName().trim();
        Integer count = gpuPhoneRepo.getNumberOfGpuPhone(name);

        if (count > 0) {
            throw new RuntimeException("Tên GPU đã tồn tại");
        }

        GpuPhone gpuPhone = GpuPhone.builder()
                .name(name)
                .build();

        gpuPhoneRepo.saveAndFlush(gpuPhone);

        return gpuPhone;
    }

    @Override
    public List<GpuRes> getAllGpus() {
        log.info("Get all gpus");
        return gpuPhoneRepo.getAllGpus();
    }

//    /**
//     * Convert Map to String using StringBuilder.
//     * @param map object needing convert.
//     * @return a string converted from Map obj
//     */
//    @Override
//    public String convertMapSpeeds(Map<String, Float> map) {
//
//        StringBuilder mapAsString = new StringBuilder();
//
//        for (String key : map.keySet()) {
//
//            mapAsString.append(key + ":" + map.get(key) + ", ");
//        }
//        mapAsString.delete(mapAsString.length() - 2, mapAsString.length());
//
//        return  mapAsString.toString();
//    }
//
//    @Override
//    public void createListOptionsPhone(OptionDto req, Integer phoneId) {
//        optionsPhoneRepo.createNewOption(req.addPrice(), req.quantity(), req.ram(),
//                req.storage(), phoneId);
//    }

    @Override
    public ScreenDto createScreenForPhone(ScreenDto req) {

        log.info("Create new screen: {}", req);

        Screen screen = Screen.builder()
                .brightest(req.getBrightest())
                .dimension(req.getDimension())
                .note(req.getNote())
                .resolution(req.getResolution())
                .tech(req.getTech())
               .build();

        screenRepo.saveAndFlush(screen);

        req.setId(screen.getScreenId());

        screenRepo.updatingPhoneForScreen(req.getPhoneId(), req.getId());

        return req;
    }
}
