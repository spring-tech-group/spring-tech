package com.inventory.service;

import com.inventory.DTO.*;

import java.util.List;

/**
 * @author thng1642
 * @version 1.00
 *
 * Modification log
 * ============================================================
 * Date          Author             Description
 * 10 Feb 2023  thng1642            Refactor code
 */
public interface DeviceService {

    List<IDeviceDto> getDevicesOnDB();

    Integer createNewPhone(PhoneDto phone, BatteryDto battery, Integer brandId, ConnectDto connect,
                           FCameraDto fcamera, GpuDto gpu, NewCpuReq cpu, List<OptionDto> options,
                           List<RCameraDto> rCameras, List<ScreenDto> screens);

    // add new phone base features
    Integer makePhoneBaseFeatures(NewPhone phone);

    void updatingPhone(UpdatePhoneDto res);

    List<CheckItemsResponse> checkStockOutItem(List<ItemDto> items);
}
