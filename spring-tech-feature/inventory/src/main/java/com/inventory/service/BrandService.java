package com.inventory.service;

import com.inventory.DTO.BrandDto;
import com.inventory.DTO.BrandResp;
import com.inventory.model.Brand;
import com.inventory.model.Producer;

import java.util.List;

public interface BrandService {
    // make new brand
    Brand createdNewBrand(BrandDto request);

    // get number of brands
    Integer getNumberBrand(String name);

    // get all brands
    List<BrandResp> getAllBrands();
}
