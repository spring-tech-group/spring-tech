package com.inventory.service;

import com.inventory.DTO.*;
import com.inventory.error.exception.UpdatingPhoneFailed;
import com.inventory.model.*;
import com.inventory.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement device service
 *
 * @author thng1642
 * @version 1.00
 *
 * Modification log
 * Date         Author          Description
 * =======================================================
 *
 */
@Service @Slf4j
@RequiredArgsConstructor @Transactional
public class DeviceServiceIml implements DeviceService{
    private final OptionsPhoneRepo optionsPhoneRepo;

    private final PhoneRepo phoneRepo;

    private final LaptopRepo laptopRepo;

    private final BrandRepo brandRepo;

    private final PhoneImgRepo phoneImgRepo;

    @Override
    public List<IDeviceDto> getDevicesOnDB() {

        log.info("Get all device to send UI");
        return laptopRepo.getAllLaptop();
    }

    @Override
    public Integer createNewPhone(PhoneDto phone, BatteryDto battery, Integer brandId, ConnectDto connect,
            FCameraDto fcamera, GpuDto gpu, NewCpuReq cpu, List<OptionDto> options, List<RCameraDto> rCameras, List<ScreenDto> screens) {

        log.info("Add new phone {}", phone.name());

        return null;
    }

    @Override
    public Integer makePhoneBaseFeatures(NewPhone phone) {

        Brand brandDetails = Brand.builder().build();

        try {
            brandDetails = brandRepo.findById(phone.brandId()).orElseThrow(
                    ()->new RuntimeException("The brand not exited")
            );
//            log.info("The brand get from db: {}", brandDetails);
        }catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }

        Phone phoneDetails = Phone.builder()
                .name(phone.name())
                .dateAt(phone.dateAt())
                .dimensions(phone.dimensions())
                .operation(phone.operation())
                .price(phone.price())
                .weigh(phone.weigh())
                .brandPhone(brandDetails)
                .build();

        phoneDetails = phoneRepo.saveAndFlush(phoneDetails);

        OptionsPhone optionsPhoneDetails = OptionsPhone.builder()
                .quantity(phone.quantity())
                .ram(phone.ram())
                .storage(phone.storage())
                .phone(phoneDetails)
                .build();
        optionsPhoneRepo.saveAndFlush(optionsPhoneDetails);

        return phoneDetails.getPhoneId();
    }

    @Override
    public void updatingPhone(UpdatePhoneDto res) {

        // updating table phone
        Integer isUpdated = phoneRepo.updatingPhone(res.getBattery_id(),res.getConnect_id(),
                res.getCpu_id(), res.getGpu_id(), res.getFcamera_id(), res.getPhone_id());

        if (isUpdated == null) {
            throw new UpdatingPhoneFailed("Cập nhật thất bại điện thoại");
        }

        // Adding images for phone

        PhoneImgDto phoneImgDto = res.getPhoneImgDto();
        log.info("Ảnh của điện thoại: {}", phoneImgDto);

        PhoneImg phoneImg = PhoneImg.builder()
                .phoneId(res.getPhone_id())
                .avt(phoneImgDto.getAvt())
                .specification(phoneImgDto.getSpecification())
                .dimension(phoneImgDto.getDimension())
                .build();
        phoneImgRepo.saveAndFlush(phoneImg);
        if (phoneImg.getPhoneId() == null) {
            throw new UpdatingPhoneFailed("Thêm ảnh điện thoại thất bại");
        }
    }

    @Override
    public List<CheckItemsResponse> checkStockOutItem(List<ItemDto> items) {

        List<CheckItemsResponse> responses = new ArrayList<>();

        boolean isOutStock = false;

        for (int i = 0; i < items.size(); i++) {

            CheckItemsResponse tmp = new CheckItemsResponse();
            Short quantityOrder = items.get(i).getQuantity();
            Integer quantityStock = optionsPhoneRepo
                    .getQuantity(items.get(i).getOptionId());
            int remain = quantityStock - quantityOrder;

            tmp.setTheRemainsOfItem(remain);

            if (remain < 0 ) isOutStock = true;

            tmp.setOptionId(items.get(i).getOptionId());

            responses.add(tmp);
        }

        if (isOutStock == false) {
            for (CheckItemsResponse item : responses) {
                OptionsPhone tmp = optionsPhoneRepo.findById(item.getOptionId())
                        .orElseThrow(()->new RuntimeException("Error at get option"));
                tmp.setQuantity(item.getTheRemainsOfItem());
//                optionsPhoneRepo.saveAndFlush(tmp);
            }
            log.info("Saving changes quantity in db.");
        }

        return responses;
    }
}
