package com.inventory.factory.device;

import com.inventory.domain.device.PhoneView;

import java.util.List;

/**
 * Where contain methods to create objects needed. The Creator.
 *
 * @author thng1642
 * @version 1.00
 *
 */
public abstract class DeviceFactory {
    public abstract PhoneView getDevice(Integer id);
    public abstract List<PhoneView> getListDevices(String brand);
}
