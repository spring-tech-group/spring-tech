package com.inventory.factory.device.concrete;

import com.inventory.domain.device.Component;
import com.inventory.domain.device.Option;
import com.inventory.domain.device.PhoneView;
import com.inventory.domain.device.PhoneViewDetails;
import com.inventory.factory.device.DeviceFactory;
import com.inventory.model.*;
import com.inventory.repository.*;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author thng1642
 * @version 1.00
 *
 * Modification log
 * Date         Author              Description
 * =============================================================
 */
@NoArgsConstructor @Service @Slf4j
public class CreatorPhoneDetails extends DeviceFactory {

    @Autowired PhoneRepo phoneRepo;

    @Autowired PhoneImgRepo phoneImgRepo;

    @Autowired ScreenRepo screenRepo;

    @Autowired RCameraRepo rCameraRepo;

    @Autowired FCameraRepo fCameraRepo;

    @Autowired CpuRepo cpuRepo;

    @Autowired OptionsPhoneRepo optionsPhoneRepo;

    @Autowired BatteryRepo batteryRepo;

    private Phone phoneModel;

    private PhoneViewDetails transferModel(Integer id) {

        PhoneViewDetails result = new PhoneViewDetails();
        String name;
        phoneModel = phoneRepo.findById(id).orElseThrow(
                () -> new RuntimeException("không tìm thấy thiết bị!")
        );
        result.setId(id);

        if (phoneModel.getBrandPhone().getBrandId() != 3) {
            name = phoneModel.getBrandPhone().getName() + " " + phoneModel.getName();
        }
        else {
            name = phoneModel.getName();
        }
        result.setName(name);
        result.setPrice(phoneModel.getPrice());

        return result;
    }

    private Component getScreen(Integer phoneId) {

        Component screen = new Component();
        Screen resultQuery = screenRepo.getScreenByPhoneId(phoneId);

        screen.setId(resultQuery.getScreenId());
        screen.setName("Màn hình");
        screen.setFeatures(resultQuery.getDimension()+ ','
                + resultQuery.getTech() + ','
                + resultQuery.getResolution());

        return screen;
    }

    private Component getRearCamera(Integer phoneId) {

        Component cam = new Component();
        RCamera rCamera = rCameraRepo.getCamByPhoneId(phoneId);

        cam.setId(rCamera.getCameraId());
        cam.setName("Camera sau");
        cam.setFeatures(rCamera.getResolution());

        return cam;
    }

    private Component getFrontCamera(Integer cameraId) {

        Component cam = new Component(cameraId, "", "");

        FCamera fCamera = fCameraRepo.findById(cameraId).orElseThrow(
                () -> new RuntimeException("Lỗi truy vấn camera trước!")
        );

        cam.setFeatures(fCamera.getResolution());
        cam.setName("Camera sau");

        return cam;
    }

    private Component getChip(Integer cpuId) {

        Component cpu = new Component(cpuId, "", "");

        CPU modelCpu = cpuRepo.findById(cpuId).orElseThrow(
                () -> new RuntimeException("Lỗi truy vấn - không tìm thấy CPU!")
        );
        cpu.setName("Chip");
        cpu.setFeatures(modelCpu.getName());

        return cpu;
    }

    private List<Component> getRamAndStorage(Integer phoneId) {

        List<OptionsPhone> optionsPhones = optionsPhoneRepo.getAllOptionsPhoneByPhoneId(phoneId);
        List<Component> result = new ArrayList<>(2);

        for(OptionsPhone item : optionsPhones) {
            if (item.getPrice() == null || item.getPrice().isEmpty()) {
                Component ram = new Component(item.getOptionsId(), "RAM", item.getRam() + "GB");
                Component storage = new Component(item.getOptionsId(),
                        "Dung lượng lưu trữ", item.getStorage() + "GB");

                result.add(ram);
                result.add(storage);

                break;
            }
        }

        return result;
    }

    private Component getBattery(Integer batteryId) {

        Battery modelBattery = batteryRepo.findById(batteryId).orElseThrow(
                () -> new RuntimeException("Lỗi truy vấn - không tìm thấy Battery!")
        );

        Component battery = new Component(batteryId, "Pin,Sạc", modelBattery.getCapacity() + "mAh" +
                ',' + modelBattery.getCharging() + "W");

        return battery;
    }

    @Override
    public PhoneView getDevice(Integer id) {

        PhoneViewDetails result = null;
        HashMap<String, List<Option>> options  = new HashMap<>();
        List<Component> specs = new ArrayList<>(8);

        // Set id for phone
        result = transferModel(id);

        PhoneImg phoneImg = phoneImgRepo.findById(id).orElseThrow(
                () -> new RuntimeException("Không tìm thấy dữ liệu, với id: " + id)
        );
        result.setDimensionImage(phoneImg.getDimension());

        result.setSlides(Arrays.asList(phoneImg.getSpecification().split(";")));

        // Set screen component
        specs.add(getScreen(id));

        // Set operation
        Component operation = new Component(phoneModel.getPhoneId(),
                "Hệ điều hành", phoneModel.getOperation());
        specs.add(operation);

        // Set rear camera
        specs.add(getRearCamera(id));

        // Set front camera
        specs.add(getFrontCamera(phoneModel.getFCamera().getCameraId()));

        // Set chip or CPU
        specs.add(getChip(phoneModel.getCpuPhone().getCpuId()));

        // Set RAM and Storage
        specs.addAll(getRamAndStorage(id));

        // Set battery and charging
        specs.add(getBattery(phoneModel.getBattery().getBatteryId()));

        // Set options phone
        // 1. Storage
        List<OptionsPhone> storages = optionsPhoneRepo.getAllOptionsPhoneByPhoneId(id);
//        log.info("Options for phone: {}", storages);
        List<Option> option = new ArrayList<>();

        if (storages.size() >= 1) {
            for(OptionsPhone item : storages) {

                Option tmp = new Option(item.getOptionsId(),item.getPrice(), item.getStorage());
                option.add(tmp);
            }
        }
        result.setStorages(option);

//        result.setOptions(options);

        result.setSpecs(specs);

        return result;
    }

    @Override
    public List<PhoneView> getListDevices(String brand) {
        return null;
    }
}
