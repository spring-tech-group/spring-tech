package com.inventory.factory.device.concrete;


import com.inventory.domain.device.PhoneView;
import com.inventory.domain.device.PhoneViewCard;
import com.inventory.factory.device.DeviceFactory;
import com.inventory.model.Phone;
import com.inventory.model.PhoneImg;
import com.inventory.repository.BrandRepo;
import com.inventory.repository.PhoneImgRepo;
import com.inventory.repository.PhoneRepo;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thng1642
 * @version 1.00
 *
 * Modification log
 * Date         Author              Description
 * =============================================================
 */
@NoArgsConstructor @Service @Slf4j
public class CreatorPhoneCard extends DeviceFactory {

    @Autowired
    PhoneRepo phoneRepo;

    @Autowired PhoneImgRepo phoneImgRepo;
    @Autowired BrandRepo brandRepo;

    @Override
    public PhoneView getDevice(Integer id) {
        return null;
    }

    @Override
    public List<PhoneView> getListDevices(String brand) {

        // Get brand id
        Integer brandId = brandRepo.getIdBrand(brand.toLowerCase());

        List<PhoneView> phoneViewCards = new ArrayList<>();

        List<Phone> phones = phoneRepo.getPhoneByBrand(brandId);

        // get and set data for PhoneView list instants
        for(Phone item : phones) {
            PhoneViewCard tmpPhoneView = new PhoneViewCard(item.getPhoneId(),
                    item.getName(), "" ,item.getPrice());

            PhoneImg tmpImg = phoneImgRepo.findById(item.getPhoneId())
                    .orElseThrow();

//            log.info("Phone id: {}", item.getPhoneId());

            tmpPhoneView.setAvt(tmpImg.getAvt());
            phoneViewCards.add(tmpPhoneView);
        }

//
        return phoneViewCards;
    }
}
