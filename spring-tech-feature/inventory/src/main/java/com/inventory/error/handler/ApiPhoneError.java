package com.inventory.error.handler;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class ApiPhoneError {
    private HttpStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyy hh:mm:ss")
    private LocalDateTime timestamp;
    private String message;
    private String debugMessage;
    // holds an array of sub-errors when there are multiple errors in a single call.
    private List<ApiSubError> subErrors;

    private ApiPhoneError() {
        timestamp = LocalDateTime.now();
    }

    ApiPhoneError(HttpStatus status) {
        this();
        this.status = status;
    }

    ApiPhoneError(HttpStatus status, Throwable ex) {
        this();
        this.status = status;
        this.message = "Unexpected error";
        this.debugMessage = ex.getLocalizedMessage();
    }

    ApiPhoneError(HttpStatus status, String message, Throwable ex) {
        this();
        this.status = status;
        this.message = message;
        this.debugMessage = ex.getLocalizedMessage();
    }
}
