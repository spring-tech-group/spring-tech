package com.inventory.error.handler;

import com.inventory.error.exception.UpdatingPhoneFailed;
import org.junit.jupiter.api.Order;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class InventoryExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
//        return super.handleHttpMessageNotReadable(ex, headers, status, request);
        String error = "Malformed JSON request";
        return buildResponseEntity(new ApiPhoneError(HttpStatus.BAD_REQUEST, error, ex));
    }

    @ExceptionHandler(UpdatingPhoneFailed.class)
    protected ResponseEntity<Object> handleUpdatingPhoneFailed(UpdatingPhoneFailed ex) {
        ApiPhoneError apiPhoneError = new ApiPhoneError(HttpStatus.BAD_REQUEST);
        apiPhoneError.setMessage(ex.getMessage());
        return buildResponseEntity(apiPhoneError);
    }

    private ResponseEntity<Object> buildResponseEntity(ApiPhoneError apiPhoneError) {
        return new ResponseEntity<>(apiPhoneError, apiPhoneError.getStatus());
    }
}
