package com.inventory.error.exception;

public class UpdatingPhoneFailed extends RuntimeException {
    public UpdatingPhoneFailed(String message) {
        super(message);
    }

}
