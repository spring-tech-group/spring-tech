package com.inventory.repository;

import com.inventory.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhoneRepo extends JpaRepository<Phone, Integer> {

    @Modifying
    @Query(value = "UPDATE phone u SET battery_id = ?1, connect_id = ?2, cpu_id = ?3, " +
            "gpu_id = ?4, fcamera_id = ?5 " +
            "WHERE u.phone_id = ?6", nativeQuery = true)
    Integer updatingPhone(Integer batteryId, Integer connectId, Integer cpuId,
                       Integer gpuId, Integer fcameraId, Integer id);

    @Query(value = "SELECT * FROM phone WHERE brand_id = ?1",
            nativeQuery = true)
    List<Phone> getPhoneByBrand(Integer brand);
}
