package com.inventory.repository;

import com.inventory.model.Battery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BatteryRepo extends JpaRepository<Battery, Integer> {
}
