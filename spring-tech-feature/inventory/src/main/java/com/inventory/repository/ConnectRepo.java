package com.inventory.repository;

import com.inventory.model.Connect;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnectRepo extends JpaRepository<Connect, Integer> {
}
