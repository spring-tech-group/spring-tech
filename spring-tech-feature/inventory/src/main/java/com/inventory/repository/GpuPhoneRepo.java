package com.inventory.repository;

import com.inventory.DTO.GpuRes;
import com.inventory.model.GpuPhone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GpuPhoneRepo extends JpaRepository<GpuPhone, Integer> {

    @Query(value = "SELECT count(gpu_phone_id) FROM gpu_phone u WHERE ?1 = lower(u.name)",
            nativeQuery = true)
    Integer getNumberOfGpuPhone(String name);

    @Query(value = "SELECT u.gpu_phone_id as id, u.name FROM gpu_phone u",
            nativeQuery = true)
    List<GpuRes> getAllGpus();
}
