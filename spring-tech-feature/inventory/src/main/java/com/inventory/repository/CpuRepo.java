package com.inventory.repository;

import com.inventory.DTO.CpuDto;
import com.inventory.model.CPU;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CpuRepo extends JpaRepository<CPU, Integer> {
    @Query(value = "SELECT * FROM cpu u WHERE u.name = ?1", nativeQuery = true)
    Optional<CPU> getCPUByName(String name);

    @Query(value = "SELECT u.cpu_id as id, u.name, u.core FROM cpu u", nativeQuery = true)
    List<CpuDto> getAllCpus();
}
