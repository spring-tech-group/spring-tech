package com.inventory.repository;

import com.inventory.DTO.IDeviceDto;
import com.inventory.model.Laptop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LaptopRepo extends JpaRepository<Laptop, Integer> {

    @Query(value = "SELECT u.laptop_id, u.name, u.avt, u.price, u.updated_date FROM laptop u", nativeQuery = true)
    List<IDeviceDto> getAllLaptop();
}
