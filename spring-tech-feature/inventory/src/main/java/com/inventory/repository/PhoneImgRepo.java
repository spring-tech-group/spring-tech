package com.inventory.repository;

import com.inventory.model.PhoneImg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneImgRepo extends JpaRepository<PhoneImg, Integer> {
}
