package com.inventory.repository;

import com.inventory.model.OptionsPhone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thng1642
 * @version 1.00
 *
 * Modification log
 * =========================================================
 * DATE         AUTHOR          DESCRIPTION
 * 24 Jan 2023  thng1642        Adding new query find options
 *                              by phone_id
 * 04 Feb 2023  thng1642        Adding query get quantity of
 *                              a phone_option.
 */
@Repository
public interface OptionsPhoneRepo extends JpaRepository<OptionsPhone, Integer> {

    @Modifying
    @Query(value = "INSERT INTO options_phone (price, quantity, ram, storage, phone_id) " +
            "VALUES (?1, ?2, ?3, ?4, ?5)", nativeQuery = true)
    void createNewOption(String price, Integer quantity, Short ram,
                            String storage, Integer phoneId);

    @Query(value = "SELECT * FROM options_phone u WHERE u.phone_id = ?1"
            , nativeQuery = true)
    List<OptionsPhone> getAllOptionsPhoneByPhoneId(Integer phoneId);

    @Query(value = "SELECT u.quantity FROM options_phone u WHERE u.options_id = ?1",
            nativeQuery = true)
    Integer getQuantity(Integer optionId);
}
