package com.inventory.repository;

import com.inventory.model.RAM;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RamRepo extends JpaRepository<RAM, Integer> {
}
