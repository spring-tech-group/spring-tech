package com.inventory.repository;

import com.inventory.model.Screen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thng1642
 * @version 1.00
 *
 * MODIFICATION LOG
 * DATE         AUTHOR          DESCRIPTION
 * =======================================================
 * 24 Jan 2023  thng1642        Adding query get screen
 *                              by phone_id
 */
@Repository
public interface ScreenRepo extends JpaRepository<Screen, Integer> {

    @Modifying
    @Query(value = "INSERT INTO screen (brightest, dimension, note, resolution, tech, phone_id)" +
            "VALUES (?1, ?2, ?3, ?4, ?5, ?6)",
        nativeQuery = true)
    void createScreenForPhone(Short brightest, String dimension, String note,
                              String resolution, String tech, Integer phoneId);

    @Modifying
    @Query(value = "UPDATE screen u SET phone_id = ?1 WHERE u.screen_id = ?2",
        nativeQuery = true)
    Integer updatingPhoneForScreen(Integer phoneId, Integer id);

    @Query(value = "SELECT * FROM screen u WHERE u.phone_id = ?1", nativeQuery = true)
    Screen getScreenByPhoneId(Integer phoneId);

}
