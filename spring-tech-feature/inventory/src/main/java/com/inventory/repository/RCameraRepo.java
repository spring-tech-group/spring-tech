package com.inventory.repository;

import com.inventory.model.RCamera;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author thng1642
 * @version 1.00
 *
 * MODIFICATION LOG
 * =======================================================================
 * DATE         AUTHOR          DESCRIPTION
 * 24 Jan 2023  thng1642        Adding query get camera by phone_id
 */
@Repository
public interface RCameraRepo extends JpaRepository<RCamera, Integer> {

    @Modifying
    @Query(value = "INSERT INTO rcamera (features, resolution, phone_id)" +
            "VALUES (?1, ?2, ?3)", nativeQuery = true)
    void createNewRCamera(String features, String resolutions, Integer phoneId);

    @Modifying
    @Query(value = "UPDATE rcamera SET phone_id = ?1 WHERE camera_id = ?2"
            , nativeQuery = true)
    Integer updatingPhoneIdForRCamera(Integer phoneId, Integer id);

    @Query(value = "SELECT * FROM rcamera u WHERE u.phone_id = ?1", nativeQuery = true)
    RCamera getCamByPhoneId(Integer phoneId);
}
