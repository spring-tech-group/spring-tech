package com.inventory.repository;

import com.inventory.DTO.BrandResp;
import com.inventory.model.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BrandRepo extends JpaRepository<Brand, Integer> {

    @Query(value = "SELECT count(brand_id) FROM brand u WHERE ?1 = lower(u.name)",
            nativeQuery = true)
    Integer getNumberOfBrand(String name);

    @Query(value = "SELECT u.brand_id as id, u.name, u.logo, u.field FROM brand u",
            nativeQuery = true)
    List<BrandResp> getBrands();
    @Query(value = "SELECT u.brand_id FROM brand u WHERE lower(u.name) = ?1",
    nativeQuery = true)
    Integer getIdBrand(String name);

}
