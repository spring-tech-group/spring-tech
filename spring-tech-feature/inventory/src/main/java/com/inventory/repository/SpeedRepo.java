package com.inventory.repository;

import com.inventory.model.Speed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpeedRepo extends JpaRepository<Speed, Integer> {
}
