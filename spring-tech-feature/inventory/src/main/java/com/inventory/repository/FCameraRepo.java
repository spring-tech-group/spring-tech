package com.inventory.repository;

import com.inventory.model.FCamera;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FCameraRepo extends JpaRepository<FCamera, Integer> {
}
