package com.inventory.repository;

import com.inventory.model.GPU;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GpuRepo extends JpaRepository<GPU, Integer> {

    @Query(value = "SELECT * FROM gpu u WHERE u.name = ?1", nativeQuery = true)
    Optional<GPU> getGPUByName(String name);
}
