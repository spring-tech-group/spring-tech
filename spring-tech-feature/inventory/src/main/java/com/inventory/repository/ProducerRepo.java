package com.inventory.repository;

import com.inventory.DTO.BrandResp;
import com.inventory.model.Producer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProducerRepo extends JpaRepository<Producer, Integer> {

    @Query(value = "SELECT * FROM producer u WHERE u.name = ?1", nativeQuery = true)
    Optional<Producer> getProducerByName(String name);

    @Query(value = "SELECT count(producer_id) FROM producer u WHERE ?1 = lower(u.name)", nativeQuery = true)
    Integer getNumberOfBrand(String name);

    @Query(value = "SELECT u.producer_id as id, u.name, u.logo FROM producer u", nativeQuery = true)
    List<BrandResp> getBrands();
}
