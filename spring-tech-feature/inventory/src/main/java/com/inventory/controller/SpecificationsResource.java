package com.inventory.controller;

import com.inventory.DTO.*;
import com.inventory.model.GpuPhone;
import com.inventory.service.SpecificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for anything relative with specs
 *
 * @author thng1642
 * @version 1.00
 *
 * Modification logs
 * Date         Author          Description
 * ===================================================================
 * 10 Feb 2023  thng1642        Refactor code: adding comments, remove
 *                              packages not use,...
 */
@RestController
@RequiredArgsConstructor @RequestMapping("api/v1/")
public class SpecificationsResource {

    private final SpecificationService service;

    /**
     * Create spec-cpu for phone if it is not existed.
     * @param req sent to client
     * @return response results
     */
    @PostMapping("create/cpu")
    public ResponseEntity<?> createNewCpu(@RequestBody CpuReq req){

        CpuReq resp = service.createNewCpu(req);
        if (resp != null) {
            return ResponseEntity.ok(resp);
        }
        return ResponseEntity.badRequest().body("Tạo CPU thất bại");
    }

    /**
     * get all cpu phone created.
     */
    @GetMapping("cpus")
    public ResponseEntity<?> getCpus() {
        List<CpuDto> result = service.getCpus();
        if (result.isEmpty()) {
            return  ResponseEntity.badRequest().body("Không tìm thấy các đối tượng");
        }
        return ResponseEntity.ok(result);
    }

    /**
     * create battery spec for phone
     * @param req sent to client
     * @return object contains id which just saved in db.
     */
    @PostMapping("create/battery")
    public ResponseEntity<?> createNewBattery(@RequestBody BatteryDto req){

        BatteryDto res = service.createBattery(req);
        if (res == null) {

            return ResponseEntity.badRequest().body("Tạo đối tượng pin thất bại");
        }

        return ResponseEntity.ok(res);
    }

    /**
     * create connect spec for phone
     * @param req sent to client
     * @return object contains id which just saved in db.
     */
    @PostMapping("create/connect")
    public ResponseEntity<?> createNewConnect(@RequestBody ConnectDto req) {

        ConnectDto res = service.createConnect(req);

        if (res == null) {
            return ResponseEntity.badRequest().body("Tạo đối tượng kết nối thất bại");
        }
        return ResponseEntity.ok(res);
    }

    /**
     * create gpu spec for phone
     * @param req sent to client
     * @return object contains id which just saved in db
     */
    @PostMapping("create/gpu/phone")
    public ResponseEntity<?> createNewGPU(@RequestBody GpuDto req) {

        GpuPhone insGpu = null;
        try {
            insGpu = service.createGpu(req);
            GpuDto response = new GpuDto(insGpu.getGpuPhoneId(), insGpu.getName());
            return ResponseEntity.ok(response);
        }catch (Exception e) {

            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    /**
     * get all cpus created before
     * @return list cups
     */
    @GetMapping("gpus/phone")
    public  ResponseEntity<?> getAllGpus(){
        return ResponseEntity.ok(service.getAllGpus());
    }

    /**
     * create cameras spec for phone
     * @param req sent to client
     * @return object contains id which just saved in db
     */
    @PostMapping("create/cameras")
    public ResponseEntity<?> createNewCamera(@RequestBody List<CameraDto> req) {
        List<CameraDto>res = service.createCameras(req);
        return ResponseEntity.ok(res);
    }

    /**
     * create screen spec for phone
     * @param req sent to client
     * @return object contains id which just saved in db
     */
    @PostMapping("create/screen")
    public ResponseEntity<?> createNewScreen(@RequestBody ScreenDto req) {
        ScreenDto res = service.createScreenForPhone(req);
        return ResponseEntity.ok(res);
    }
}
