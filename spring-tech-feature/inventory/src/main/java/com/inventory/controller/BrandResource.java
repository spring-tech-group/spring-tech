package com.inventory.controller;

import com.inventory.DTO.BrandDto;
import com.inventory.DTO.BrandResp;
import com.inventory.model.Brand;
import com.inventory.service.BrandService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for anything relative with devices
 *
 * @author thng1642
 * @version 1.00
 *
 * Modification log
 * Date             Author          Description
 * ====================================================================
 * 10 Feb 2023      thng1642        Refactor code
 */
@RestController @RequiredArgsConstructor
@RequestMapping("api/v1/")
public class BrandResource {

    private final BrandService service;

    /**
     * Adding new brand in system
     * @param brandDto sent to client
     * @return object contains id which just saved in db
     */
    @PostMapping(value = "brand")
    public ResponseEntity<?> makeNewBrand(@RequestBody BrandDto brandDto) {

        Brand result = service.createdNewBrand(brandDto);

        if (result == null) {
             return ResponseEntity.badRequest()
                     .body("Thương hiệu " + brandDto.name() + " đã tồn tại trong cơ sở dữ liệu.");
        }
        else {
            return ResponseEntity.ok("Thêm thành công thương hiệu " + brandDto.name() + ".");
        }
    }

    /**
     * Get all brands created before.
     * @return list of brands
     */
    // TODO: temporary name api
    @GetMapping(value = "all")
    public  ResponseEntity<?> test() {

        List<BrandResp> result = service.getAllBrands();

        if (result == null) {
            return ResponseEntity.badRequest().body("Loi truy van");

        }
        else {
            return ResponseEntity.ok(result);
        }
    }
}
