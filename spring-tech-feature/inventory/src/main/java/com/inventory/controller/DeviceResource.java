package com.inventory.controller;

import com.inventory.DTO.*;
import com.inventory.factory.device.concrete.CreatorPhoneCard;
import com.inventory.factory.device.concrete.CreatorPhoneDetails;
import com.inventory.service.DeviceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/")
@Slf4j
public class DeviceResource {

    private final DeviceService deviceService;
    private final CreatorPhoneCard creatorPhoneCard;
    private final CreatorPhoneDetails creatorPhoneDetails;

    @GetMapping("/items")
    public ResponseEntity<?> addProducer() {

        return ResponseEntity.ok(deviceService.getDevicesOnDB());
    }

    /**
     * Create base info phone such as: name, operation, price, ...
     * @param req contains base info
     * @return
     */
    //TODO: get code at api /test to replace. After, Change at client.
    @PostMapping("/create/phone")
    public ResponseEntity<?> addNewPhone(@RequestBody NewPhoneReq req) {

        Integer result = deviceService
                .createNewPhone(req.phone(), req.battery(), req.brandId(),
                        req.connect(), req.fcamera(), req.gpu(), req.cpu(),
                        req.options(), req.rCameras(),
                        req.screens());

        if (result != null) {
            return ResponseEntity.ok("Tạo điện thoại thành công");
        }
        else {
            return ResponseEntity.badRequest().body("Tạo điện thoại thất bại");
        }
    }

    @PostMapping("update/phone")
    public ResponseEntity<?> updatePhone(@RequestBody UpdatePhoneDto res) {

        deviceService.updatingPhone(res);
        return ResponseEntity.ok(res);
    }
    @PostMapping("/test")
    public ResponseEntity<?> test(@RequestBody NewPhone req) {
        log.info("Data sent from client: {}", req);

        Integer  phoneId = deviceService.makePhoneBaseFeatures(req);

        return ResponseEntity.ok(phoneId);
    }

    /**
     * Get card object from gives a brand
     * @param brand sent to client
     * @return list card necessary
     */
    @GetMapping("phonecard")
    public ResponseEntity<?> getPhoneCard(@RequestParam("brand") String brand) {

//        creatorPhoneCard.getListDevices(brand);
        return ResponseEntity.ok(creatorPhoneCard.getListDevices(brand));
    }

    /**
     * Show up details info/specs phone
     * @param id id phone
     */
    @GetMapping("phone")
    public ResponseEntity<?> getPhoneDetails(@RequestParam("phone") Integer id) {
        return ResponseEntity.ok(creatorPhoneDetails.getDevice(id));
    }

    /**
     * Check stock-out when customer actions order items
     * @param req sent to order services
     * @return result checking
     */
    @PostMapping("items-check")
    public ResponseEntity<List<CheckItemsResponse>> checkItemInStock(@RequestBody List<ItemDto> req) {
        // check stock-out of items
        return ResponseEntity.ok().body(deviceService.checkStockOutItem(req));
    }
}
