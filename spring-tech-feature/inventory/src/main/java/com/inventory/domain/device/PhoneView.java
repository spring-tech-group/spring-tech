package com.inventory.domain.device;

/**
 * Supper Class for objects, which client need to show content at React
 *
 * @author thng1642
 * @version 1.00
 *
 */
public interface PhoneView {
}
