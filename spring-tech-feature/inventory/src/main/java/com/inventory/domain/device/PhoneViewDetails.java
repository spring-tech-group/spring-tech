package com.inventory.domain.device;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * The class represents a phone details show at client (React).
 * @author thng1642
 * @version 1.00
 *
 * MODIFICATION LOG
 * DATE         AUTHOR          DESCRIPTION
 * =======================================================
 * 23 Jan 2023  thng1642        Adding fields
 */
@Data @NoArgsConstructor @AllArgsConstructor
public class PhoneViewDetails implements PhoneView {
    private Integer id;
    private String name;
    private String price;
    private String dimensionImage;
    private List<String> slides;
    private List<Option> storages;
    private List<Component> specs;
}
