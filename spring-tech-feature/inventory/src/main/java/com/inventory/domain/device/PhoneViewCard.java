package com.inventory.domain.device;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The class represents a Card phone at client React.
 *
 * @author thng1642
 * @version 1.00
 *
 */
@Data @NoArgsConstructor @AllArgsConstructor
public class PhoneViewCard implements PhoneView {
    private Integer id;
    private String name;
    private String avt;
    private String price;
}
