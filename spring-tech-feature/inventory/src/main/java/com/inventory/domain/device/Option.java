package com.inventory.domain.device;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor @AllArgsConstructor @Data
public class Option {
    private Integer id;
    private String price;
//    private String ram;
    private String storage;
}
