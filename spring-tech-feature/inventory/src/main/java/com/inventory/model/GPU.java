package com.inventory.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GPU {

    @Id
    @SequenceGenerator(
            name = "gpu_id_sequence",
            sequenceName = "gpu_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "gpu_id_sequence"
    )
    private Integer gpuId;

    private String name;

    private boolean onBoard;

    private String vRam;

//    @OneToMany(mappedBy = "gpu")
//    private List<PhoneView> phones;

    @ManyToMany(mappedBy = "gpus",cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    private List<Laptop> laptops;
}
