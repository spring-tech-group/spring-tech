package com.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RAM {

    @Id
    @SequenceGenerator(
            name = "ram_id_sequence",
            sequenceName = "ram_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "ram_id_sequence"
    )
    private Integer ramId;

    private short capacity;

    private short bus;

    private String tech;

    private boolean isPhone;

    @OneToOne(mappedBy = "ram")
    private Laptop laptop;
}
