package com.inventory.model;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Speed {
    @Id
    @SequenceGenerator(
            name = "speed_id_sequence",
            sequenceName = "speed_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "speed_id_sequence"
    )
    private Integer speedId;

    private String name;        // Single or multiple core

    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cpu_id")
    private CPU cpu;
}
