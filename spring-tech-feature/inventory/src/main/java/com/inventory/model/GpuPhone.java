package com.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GpuPhone {
    @Id
    @SequenceGenerator(
            name = "gpu_phone_id_sequence",
            sequenceName = "gpu_phone_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "gpu_phone_id_sequence"
    )
    private Integer gpuPhoneId;

    private String name;

    @OneToMany(mappedBy = "gpu", cascade = CascadeType.ALL)
    private List<Phone> phones;
}
