package com.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FCamera {

    @Id
    @SequenceGenerator(
            name = "f_camera_id_sequence",
            sequenceName = "f_camera_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "f_camera_id_sequence"
    )
    private Integer cameraId;

    private String resolution;

    private String features;

    @OneToOne(mappedBy = "fCamera")
    private Phone phone;

    @OneToOne(mappedBy = "fCamera")
//    @JoinColumn(name = "laptop_id")
    private Laptop laptop;
}
