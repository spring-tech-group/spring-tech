package com.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Connect {

    @Id
    @SequenceGenerator(
            name = "connect_id_sequence",
            sequenceName = "connect_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "connect_id_sequence"
    )
    private Integer connectId;

    private String mobileNetwork;

    private String bluetooth;

    private String wifi;

    @OneToMany(mappedBy = "connect", cascade=CascadeType.ALL)
    private List<Phone> phones;

    @OneToMany(mappedBy = "connect", cascade=CascadeType.ALL)
    private List<Laptop> laptops;

}
