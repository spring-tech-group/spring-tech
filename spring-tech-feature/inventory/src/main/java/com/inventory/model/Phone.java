package com.inventory.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Phone {

    @Id
    @SequenceGenerator(
            name = "phone_id_sequence",
            sequenceName = "phone_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "phone_id_sequence"
    )
    private Integer phoneId;

    private String name;

    private String dimensions;

    @Column(length = 5)
    private String weigh;

    private LocalDate dateAt;

    private String operation;

    private String price;

    private boolean deleted;

    @OneToMany(mappedBy = "phone", cascade=CascadeType.ALL)
    private List<OptionsPhone> options;

//    @ManyToOne
//    @JoinColumn(name = "producer_id")
//    private Producer producerPhone;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brandPhone;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cpu_id")
    private CPU cpuPhone;

    @ManyToOne
    @JoinColumn(name = "gpu_id")
    private GpuPhone gpu;

    @OneToMany(mappedBy = "phone", cascade=CascadeType.ALL)
    private List<Screen> screens;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fcamera_id", referencedColumnName = "cameraId")
    private FCamera fCamera;

    @OneToMany(mappedBy = "phone", cascade=CascadeType.ALL)
    private List<RCamera> rCameras;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "battery_id")
    private Battery battery;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "connect_id")
    private Connect connect;
}
