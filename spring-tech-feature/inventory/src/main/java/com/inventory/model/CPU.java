package com.inventory.model;

import lombok.*;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CPU {

    @Id
    @SequenceGenerator(
            name = "cpu_id_sequence",
            sequenceName = "cpu_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "cpu_id_sequence"
    )
    private Integer cpuId;

    private String name;

    private float cached;

    private short core;

    private String fastest;

    private String speeds;

    private short thread;

//    @OneToMany(mappedBy = "cpu", cascade = CascadeType.ALL)
//    private List<Speed> speeds = new java.util.ArrayList<>();

    @OneToMany(mappedBy = "cpuLaptop", cascade = CascadeType.ALL)
    private List<Laptop> laptops;

    @OneToMany(mappedBy = "cpuPhone", cascade = CascadeType.ALL)
    private List<Phone> phones;

//    @ManyToOne
//    @JoinColumn(name = "producer_id")
//    private Producer producerCPU;

}
