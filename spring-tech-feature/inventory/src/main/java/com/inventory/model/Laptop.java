package com.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Laptop {

    @Id
    @SequenceGenerator(
            name = "laptop_id_sequence",
            sequenceName = "laptop_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "laptop_id_sequence"
    )
    private Integer laptopId;

    private String name;

    private String dimensions;

    @Column(length = 5)
    private String weight;

    private String operation;

    private float price;

    private LocalDate updatedDate;

    private String avt;

//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "producer_id")
//    private Producer producerLaptop;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "brand_id")
    private Brand brandLaptop;

    @ManyToOne
    @JoinColumn(name = "cpu_id")
    private CPU cpuLaptop;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "graphic",
            joinColumns = @JoinColumn(name = "laptop_id", referencedColumnName = "laptopId"),
            inverseJoinColumns = @JoinColumn(name = "gpu_id", referencedColumnName = "gpuId"))
    private List<GPU> gpus = new ArrayList<>();

    @OneToMany(mappedBy = "laptop", cascade = CascadeType.ALL)
    private List<Screen> screens;

//    @OneToMany(mappedBy = "laptop", cascade=CascadeType.ALL)
//    private List<OptionsPhone> options;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fcamera_id", referencedColumnName = "cameraId")
    private FCamera fCamera;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ram_id", referencedColumnName = "ramId")
    private RAM ram;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "battery_id")
    private Battery battery;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "connect_id")
    private Connect connect;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "category_id")
    private Category category;
}
