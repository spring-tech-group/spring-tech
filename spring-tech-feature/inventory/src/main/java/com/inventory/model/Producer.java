package com.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Producer {

    @Id
    @SequenceGenerator(
            name = "producer_id_sequence",
            sequenceName = "producer_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "producer_id_sequence"
    )
    private Integer producerId;

    private String name;

//    private String introduction;

//    private String logo;            // saving link img logo of producer.

//    @OneToMany(mappedBy = "producerPhone", cascade=CascadeType.ALL)
//    private List<PhoneView> phones = new ArrayList<>();

//    @OneToMany(mappedBy = "producerLaptop", cascade = CascadeType.ALL)
//    private List<Laptop> laptops = new ArrayList<>();

//    @OneToMany(mappedBy = "producerCPU")
//    private List<CPU> cpus = new ArrayList<>();
}
