package com.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Battery {
    @Id
    @SequenceGenerator(
            name = "battery_id_sequence",
            sequenceName = "battery_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "battery_id_sequence"
    )
    private Integer batteryId;

    private String capacity;

    private String charging;

    private String tech;

    private String type;

    @OneToMany(mappedBy = "battery")
    private List<Phone> phones;

    @OneToMany(mappedBy = "battery")
    private List<Laptop> laptops;
}
