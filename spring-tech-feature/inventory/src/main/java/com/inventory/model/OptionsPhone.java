package com.inventory.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OptionsPhone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer optionsId;

    private String price;

    private String storage;

    private short ram;

    private int quantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "phone_id")
    private Phone phone;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "laptop_id")
//    private Laptop laptop;
}
