package com.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PhoneImg {

    @Id
    private Integer phoneId;

    private String avt;

    private String dimension;

    @Column(columnDefinition = "TEXT")
    private String specification;
}
